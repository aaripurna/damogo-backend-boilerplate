use crate::context::Ctx;
use juniper::{graphql_object, FieldResult};
use crate::models::{
    user::{CreateUser, User},
    store::{Store, CreateStore},
    order::{Order, CreateOrder},
    vendor::{Vendor, CreateVendor}
};

pub struct Mutations;

#[graphql_object(Context = Ctx)]
impl Mutations {
    async fn create_user(input: CreateUser, context: &Ctx) -> FieldResult<User> {
        Ok(context.user_repository().create(input).await?)
    }

    async fn create_store(input: CreateStore, context: &Ctx) -> FieldResult<Store> {
        Ok(context.store_repository().create(input).await?)
    }

    async fn create_order(input: CreateOrder, context: &Ctx) -> FieldResult<Order> {
        Ok(context.order_repository().create(input).await?)
    }

    async fn create_vendor(input: CreateVendor, context: &Ctx) -> FieldResult<Vendor> {
        Ok(context.vendor_repository().create(input).await?)
    }
}
