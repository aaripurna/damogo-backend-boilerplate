use crate::context::Ctx;
use juniper::{graphql_object, FieldResult};
use chrono::NaiveDateTime;

use crate::models::{
    user::User,
    store::Store,
    order::{Order, OrderType},
    vendor::Vendor,
};

pub struct Root;

#[graphql_object(Context = Ctx)]
impl Root {
    async fn users(context: &Ctx) -> FieldResult<Vec<User>> {
        Ok(context.user_repository().all().await?)
    }

    async fn user(id: i32, context: &Ctx) -> FieldResult<User> {
        Ok(context.user_repository().find(id).await?)
    }

    async fn stores(context: &Ctx) -> FieldResult<Vec<Store>> {
        Ok(context.store_repository().all().await?)
    }

    async fn store(id: i32, context: &Ctx) -> FieldResult<Store> {
        Ok(context.store_repository().find(id).await?)
    }

    async fn orders(context: &Ctx) -> FieldResult<Vec<Order>> {
        Ok(context.order_repository().all().await?)
    }

    async fn order(id: i32, context: &Ctx) -> FieldResult<Order> {
        Ok(context.order_repository().find(id).await?)
    }

    async fn vendors(context: &Ctx) -> FieldResult<Vec<Vendor>> {
        Ok(context.vendor_repository().all().await?)
    }

    async fn vendor(id: i32, context: &Ctx) -> FieldResult<Vendor> {
        Ok(context.vendor_repository().find(id).await?)
    }
}

#[graphql_object(Context = Ctx)]
impl User {
    pub fn id(&self) -> i32 {
        self.id
    }
    pub fn username(&self) -> &str {
        self.username.as_str()
    }
    pub fn email(&self) -> &str {
        self.email.as_str()
    }
    pub fn password_hash(&self) -> &str {
        self.password_hash.as_str()
    }
    pub fn created_at(&self) -> NaiveDateTime {
        self.created_at
    }
    pub fn updated_at(&self) -> NaiveDateTime {
        self.updated_at
    }

    pub async fn orders(&self, context: &Ctx) -> FieldResult<Vec<Order>> {
        Ok(context.user_repository().orders(self.id).await?)
    }
}

#[graphql_object(Context = Ctx)]
impl Vendor {
    pub fn id(&self) ->  i32 {
        self.id
    }

    pub fn username(&self) ->  &str {
        self.username.as_str()
    }

    pub fn email(&self) ->  &str {
        self.email.as_str()
    }

    pub fn password_hash(&self) ->  &str {
        self.password_hash.as_str()
    }

    pub fn created_at(&self) ->  NaiveDateTime {
        self.created_at
    }

    pub fn updated_at(&self) ->  NaiveDateTime {
        self.updated_at
    }

    pub async fn stores(&self, context: &Ctx) -> FieldResult<Vec<Store>> {
        Ok(context.vendor_repository().stores(self.id).await?)
    }

    pub async fn orders(&self, context: &Ctx) -> FieldResult<Vec<Order>> {
        Ok(context.vendor_repository().orders(self.id).await?)
    }
}

#[graphql_object(Context = Ctx)]
impl Store {
    pub fn id(&self) -> i32 {
        self.id
    }

    pub fn name(&self) -> &str {
        self.name.as_str()
    }

    pub fn vendor_id(&self) -> i32 {
        self.vendor_id
    }

    pub fn created_at(&self) -> NaiveDateTime {
        self.created_at
    }

    pub fn updated_at(&self) -> NaiveDateTime {
        self.updated_at
    }
    
    pub async fn vendor(&self, context: &Ctx) -> FieldResult<Vendor> {
        Ok(context.store_repository().vendor(self.vendor_id).await?)
    }
}

#[graphql_object(Context = Ctx)]
impl Order {
    pub fn id(&self) -> &i32 {
         &self.id
    }

    pub fn description(&self) -> &str {
         self.description.as_str()
    }

    pub fn order_type(&self) -> Option<OrderType> {
         match &self.order_type {
             0 => Some(OrderType::PICKUP),
             1 => Some(OrderType::DROPOFF),
             _ => None
         }
    }

    pub fn store_id(&self) -> i32 {
         self.store_id
    }

    pub fn user_id(&self) -> i32 {
         self.user_id
    }

    pub fn created_at(&self) -> NaiveDateTime {
         self.created_at
    }

    pub fn updated_at(&self) -> NaiveDateTime {
         self.updated_at
    }

    pub async fn store(&self, context: &Ctx) -> FieldResult<Store> {
        Ok(context.order_repository().store(self.store_id).await?)
    }

    pub async fn user(&self, context: &Ctx) -> FieldResult<User> {
        Ok(context.order_repository().user(self.user_id).await?)
    }
}