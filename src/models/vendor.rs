use serde::{Serialize, Deserialize};
use sqlx::{FromRow};
use juniper::{GraphQLInputObject};
use chrono::NaiveDateTime;

#[derive(FromRow, Serialize, Deserialize)]
pub struct Vendor{
    pub id: i32,
    pub username: String,
    pub email: String,
    pub password_hash: String,
    pub created_at: NaiveDateTime,
    pub updated_at: NaiveDateTime
}

#[allow(clippy::pedantic)]
#[derive(GraphQLInputObject)]
pub struct CreateVendor{
    pub username: String,
    pub email: String,
    pub password: String
}