use serde::{Serialize, Deserialize};
use sqlx::{FromRow};
use juniper::{GraphQLInputObject};
use chrono::NaiveDateTime;

#[derive(FromRow, Serialize, Deserialize)]
pub struct Store {
    pub id: i32,
    pub name: String,
    pub vendor_id: i32,
    pub created_at: NaiveDateTime,
    pub updated_at: NaiveDateTime,
}

#[allow(clippy::pedantic)]
#[derive(GraphQLInputObject)]
pub struct CreateStore {
    pub name: String,
    pub vendor_id: i32,
}