use serde::{Serialize, Deserialize};
use sqlx::{FromRow};
use juniper::{GraphQLInputObject};
use chrono::NaiveDateTime;

#[allow(clippy::pedantic)]
#[derive(juniper::GraphQLEnum)]
pub enum OrderType {
    PICKUP,
    DROPOFF
}

#[derive(FromRow, Serialize, Deserialize)]
pub struct Order {
    pub id: i32,
    pub description: String,
    pub order_type: i32,
    pub store_id: i32,
    pub user_id: i32,
    pub created_at: NaiveDateTime,
    pub updated_at: NaiveDateTime,
}

#[derive(GraphQLInputObject)]
#[allow(clippy::pedantic)]
pub struct CreateOrder {
    pub description: String,
    pub order_type: OrderType,
    pub store_id: i32,
    pub user_id: i32,
}

impl CreateOrder {
    pub fn converted_order_type(&self) -> i32 {
        match self.order_type {
            OrderType::PICKUP => 0,
            OrderType::DROPOFF => 1
        }
    }
}
