use sqlx::MySqlPool;
use std::fmt::Debug;
use crate::repositories::{
    user_repository::UserRepository,
    store_repository::StoreRepository,
    order_repository::OrderRepository,
    vendor_repository::VendorRepository
};

#[derive(Clone)]
pub struct Ctx {
    pub pool: MySqlPool,
    // pub scalar: juniper::DefaultScalarValue
}

impl Debug for Ctx {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str("Context")
    }
}

impl Ctx {
    pub async fn init() -> sqlx::Result<Self> {
        Ok(Self {
            pool: MySqlPool::connect(env!("DATABASE_URL")).await?,
        })
    }

    pub fn user_repository(&self) -> UserRepository {
        UserRepository::new(self.pool.clone())
    }

    pub fn store_repository(&self) -> StoreRepository {
        StoreRepository::new(self.pool.clone())
    }

    pub fn order_repository(&self) -> OrderRepository {
        OrderRepository::new(self.pool.clone())
    }

    pub fn vendor_repository(&self) -> VendorRepository {
        VendorRepository::new(self.pool.clone())
    }
}

impl juniper::Context for Ctx {}