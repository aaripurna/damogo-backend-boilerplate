use crate::models::{
    store::{Store},
    user::{User},
    order::{Order, CreateOrder},
    vendor::{Vendor}
};
use sqlx::{MySqlPool, Result, query_as, query};
use std::convert::TryInto;

#[derive(Debug, Clone)]
pub struct OrderRepository {
    pool: MySqlPool
}

impl OrderRepository {
    pub fn new(pool: MySqlPool) -> Self {
        Self { pool }
    }

    pub async fn all(&self) -> Result<Vec<Order>> {
        Ok(query_as::<_, Order>("SELECT * FROM orders").fetch_all(&self.pool).await?)
    }

    pub async fn find(&self, id: i32) -> Result<Order> {
        Ok(query_as::<_, Order>("SELECT * FROM orders WHERE id = ?")
                .bind(id)
                .fetch_one(&self.pool)
                .await?
        )
    }

    pub async fn create(&self, create_order: CreateOrder) -> Result<Order> {
        #[allow(clippy::pedantic)]
        let user = query_as::<_, User>("SELECT * FROM users WHERE id = ?")
                    .bind(&create_order.user_id)
                    .fetch_one(&self.pool)
                    .await?;

        #[allow(clippy::pedantic)]
        let store = query_as::<_, Store>("SELECT * FROM stores WHERE id = ?")
                    .bind(&create_order.store_id)
                    .fetch_one(&self.pool)
                    .await?;

        let query = query("INSERT INTO orders (order_type, description, user_id, store_id) VALUES(?, ?, ?, ?)")
                    .bind(create_order.converted_order_type())
                    .bind(create_order.description)
                    .bind(user.id)
                    .bind(store.id)
                    .execute(&self.pool)
                    .await?;
        Ok(self.find(query.last_insert_id().try_into().unwrap()).await?)
    }

    pub async fn store(&self, store_id: i32) -> Result<Store> {
        Ok(query_as::<_, Store>("SELECT * FROM stores WHERE id = ?").bind(store_id).fetch_one(&self.pool).await?)
    }

    pub async fn user(&self, user_id: i32) -> Result<User> {
        Ok(query_as::<_, User>("SELECT * FROM users WHERE id = ?").bind(user_id).fetch_one(&self.pool).await?)
    }
}