use crate::models::{
    user::{User, CreateUser},
    order::{Order}
};
use sqlx::{MySqlPool, Result, query_as, query};
use bcrypt::{DEFAULT_COST, hash};
use std::convert::TryInto;

#[derive(Debug, Clone)]
pub struct UserRepository {
    pool: MySqlPool
}

impl UserRepository {
    pub fn new(pool: MySqlPool) -> Self {
        Self { pool }
    }

    pub async fn all(&self) -> Result<Vec<User>> {
        Ok(query_as::<_, User>("SELECT * FROM users").fetch_all(&self.pool).await?)
    }

    pub async fn find(&self, id: i32) -> Result<User> {
        Ok(query_as::<_, User>("SELECT * FROM users WHERE id = ?").bind(id).fetch_one(&self.pool).await?)
    }

    pub async fn orders(&self, user_id: i32) -> Result<Vec<Order>> {
        Ok(query_as::<_, Order>("SELECT * FROM orders WHERE user_id = ? ORDER BY created_at DESC").bind(user_id).fetch_all(&self.pool).await?)
    }

    pub async fn create(&self, user: CreateUser) -> Result<User> {
        let hashed_password = hash(user.password, DEFAULT_COST);

        #[allow(clippy::pedantic)]
        let hashed_password = match hashed_password {
            Ok(password) => password,
            Err(_) => panic!("error hasing")
        };

        let query = query("INSERT INTO users (email, username, password_hash) VALUES(?, ?, ?)")
                        .bind(&user.email)
                        .bind(&user.username)
                        .bind(&hashed_password)
                        .execute(&self.pool)
                        .await?;
        let user = self.find(query.last_insert_id().try_into().unwrap()).await?;
        Ok(user)
    }
}