use crate::models::{
    store::{Store, CreateStore},
    vendor::{Vendor}
};
use sqlx::{MySqlPool, Result, query_as, query};
use std::convert::TryInto;

#[derive(Debug, Clone)]
pub struct StoreRepository {
    pool: MySqlPool,
}

impl StoreRepository {
    pub fn new(pool: MySqlPool) -> Self {
        Self { pool }
    }

    pub async fn all(&self) -> Result<Vec<Store>> {
        Ok(query_as::<_, Store>("SELECT * FROM stores")
                .fetch_all(&self.pool)
                .await?
        )
    }

    pub async fn find(&self, id: i32) -> Result<Store> {
        Ok(query_as::<_, Store>("SELECT * FROM stores WHERE id = ?")
                    .bind(id)
                    .fetch_one(&self.pool)
                    .await?
        )
    }

    pub async fn vendor(&self, vendor_id: i32) -> Result<Vendor> {
        Ok(query_as::<_, Vendor>("SELECT * FROM vendors WHERE id = ?")
                    .bind(vendor_id)
                    .fetch_one(&self.pool)
                    .await?
        )
    }

    pub async fn create(&self, create_store: CreateStore) -> Result<Store> {
        let vendor = query_as::<_, Vendor>("SELECT * FROM vendors WHERE id = ?")
                            .bind(&create_store.vendor_id)
                            .fetch_one(&self.pool)
                            .await?;
        
        let query = query("INSERT INTO stores (name, vendor_id) VALUES(?, ?)")
                    .bind(&create_store.name)
                    .bind(&vendor.id)
                    .execute(&self.pool)
                    .await?;

        Ok(self.find(query.last_insert_id().try_into().unwrap()).await?)
    }
}