use crate::models::{
    vendor::{Vendor, CreateVendor},
    order::{Order},
    store::{Store}
};
use sqlx::{MySqlPool, Result, query_as, query};
use bcrypt::{DEFAULT_COST, hash};
use std::convert::TryInto;

#[derive(Debug, Clone)]
pub struct VendorRepository {
    pool: MySqlPool
}

impl VendorRepository {
    pub fn new(pool: MySqlPool) -> Self {
        Self { pool }
    }

    pub async fn all(&self) -> Result<Vec<Vendor>> {
        Ok(query_as::<_, Vendor>("SELECT * FROM vendors").fetch_all(&self.pool).await?)
    }

    pub async fn find(&self, id: i32) -> Result<Vendor> {
        let vendor = query_as::<_, Vendor>("SELECT * FROM vendors WHERE id = ?").bind(id).fetch_one(&self.pool).await?;
        Ok(vendor)
    }

    pub async fn create(&self, vendor: CreateVendor) -> Result<Vendor> {
        let hashed_password = hash(vendor.password, DEFAULT_COST);

        #[allow(clippy::pedantic)]
        let hashed_password = match hashed_password {
            Ok(password) => password,
            Err(_) => panic!("error hasing")
        };

        let query = query("INSERT INTO vendors (email, username, password_hash) VALUES(?, ?, ?)")
                        .bind(&vendor.email)
                        .bind(&vendor.username)
                        .bind(&hashed_password)
                        .execute(&self.pool)
                        .await?;
        let vendor = self.find(query.last_insert_id().try_into().unwrap()).await?;
        Ok(vendor)
    }

    pub async fn orders(&self, vendor_id: i32) -> Result<Vec<Order>> {
        Ok(query_as::<_, Order>("
                SELECT orders.* FROM orders
                    INNER JOIN stores ON stores.id = orders.store_id
                WHERE stores.vendor_id = ? ORDER BY orders.created_at DESC"
            ).bind(vendor_id)
            .fetch_all(&self.pool)
            .await?
        )
    }

    pub async fn stores(&self, vendor_id: i32) -> Result<Vec<Store>> {
        Ok(query_as::<_, Store>("SELECT * FROM stores WHERE vendor_id = ? ORDER BY created_at DESC").bind(vendor_id).fetch_all(&self.pool).await?)
    }
}