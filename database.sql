CREATE TABLE IF NOT EXISTS users (
    id INT(32) NOT NULL AUTO_INCREMENT,
    email VARCHAR(255) NOT NULL,
    username VARCHAR(255) NOT NULL,
    password_hash VARCHAR(255) NOT NULL,
    created_at DATETIME DEFAULT NOW(),
    updated_at DATETIME DEFAULT NOW(),

    -- CONSTRAINTS
    CONSTRAINT pk_users PRIMARY KEY (id),
    CONSTRAINT uc_users UNIQUE (id,email,username)
);

CREATE TABLE IF NOT EXISTS vendors (
    id INT(32) NOT NULL AUTO_INCREMENT,
    email VARCHAR(255) NOT NULL,
    username VARCHAR(255) NOT NULL,
    password_hash VARCHAR(255) NOT NULL,
    created_at DATETIME DEFAULT NOW(),
    updated_at DATETIME DEFAULT NOW(),

    -- CONSTRAINTS
    CONSTRAINT pk_vendors PRIMARY KEY (id),
    CONSTRAINT uc_vendors UNIQUE (id,email,username)
);

CREATE TABLE IF NOT EXISTS stores (
    id INT(32) NOT NULL AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL,
    vendor_id INT(32),
    created_at DATETIME DEFAULT NOW(),
    updated_at DATETIME DEFAULT NOW(),

    -- CONSTRAINTS
    CONSTRAINT pk_stores PRIMARY KEY (id),
    CONSTRAINT uc_stores UNIQUE(id, name),
    CONSTRAINT fk_stores_vendors FOREIGN KEY (vendor_id) REFERENCES vendors(id) ON DELETE CASCADE
);


CREATE TABLE IF NOT EXISTS orders (
    id INT(32) NOT NULL AUTO_INCREMENT,
    order_type INT,
    description TEXT,
    created_at DATETIME DEFAULT NOW(),
    updated_at DATETIME DEFAULT NOW(),
    store_id INT(32) NOT NULL,
    user_id INT(32) NOT NULL,

    -- CONSTRAINTS
    CONSTRAINT pk_orders PRIMARY KEY (id),
    CONSTRAINT fk_orders_stores FOREIGN KEY (store_id) REFERENCES stores(id) ON DELETE CASCADE,
    CONSTRAINT fk_orders_users FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE
);

CREATE UNIQUE INDEX IF NOT EXISTS users_email_index ON users(email);
CREATE UNIQUE INDEX IF NOT EXISTS users_username_index ON users(username);